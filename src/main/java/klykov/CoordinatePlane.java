package klykov;

import java.util.Scanner;

public class CoordinatePlane {
    private final int indexOfX = 0;
    private final int indexOfY = 1;

    public String askCoordinatesAndFindQuarter() {
        Scanner scan = new Scanner(System.in);
        String output = scan.nextLine();
        String[] arr = output.split(" ");
        double x = Double.parseDouble(arr[indexOfX]);
        double y = Double.parseDouble(arr[indexOfY]);
        return findQuarter(x, y);
    }



    private String findQuarter(double x, double y) {
        String quarter;

        if (x == 0) {
            quarter = (y == 0) ? "Начало координат" : "Ось ординат";
            return quarter;
        } else if (y == 0) {
            return "Ось абсцисс";
        } else if (x > 0) {
            quarter = (y > 0) ? "I" : "IV";
            return quarter;
        } else {
            quarter = (y > 0) ? "II" : "III";
            return quarter;
        }
    }
}