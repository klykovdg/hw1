package klykov;

public class Main {

    public static void main(String[] args) {
        CoordinatePlane coordinate = new CoordinatePlane();
        System.out.println("Через пробел введите координаты точки x и y");
        String answer = coordinate.askCoordinatesAndFindQuarter();
        System.out.println(answer);
    }
}