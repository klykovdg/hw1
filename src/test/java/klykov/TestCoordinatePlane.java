package klykov;

import org.junit.Assert;
import org.junit.Test;
import java.io.ByteArrayInputStream;

public class TestCoordinatePlane {
    CoordinatePlane coordinate = new CoordinatePlane();

    @Test
    public void testFirstQuarterSuccess() {
        String expected = "I";
        String userInput = "12 13";
        System.setIn(new ByteArrayInputStream(userInput.getBytes()));
        Assert.assertEquals(expected, coordinate.askCoordinatesAndFindQuarter());
    }

    @Test
    public void testSecondQuarterSuccess() {
        String expected = "II";
        String userInput = "-12 13";
        System.setIn(new ByteArrayInputStream(userInput.getBytes()));
        Assert.assertEquals(expected, coordinate.askCoordinatesAndFindQuarter());
    }

    @Test
    public void testThirdQuarterSuccess() {
        String expected = "III";
        String userInput = "-12 -13";
        System.setIn(new ByteArrayInputStream(userInput.getBytes()));
        Assert.assertEquals(expected, coordinate.askCoordinatesAndFindQuarter());
    }

    @Test
    public void testForthQuarterSuccess() {
        String expected = "IV";
        String userInput = "12 -13";
        System.setIn(new ByteArrayInputStream(userInput.getBytes()));
        Assert.assertEquals(expected, coordinate.askCoordinatesAndFindQuarter());
    }

    @Test
    public void testXAxisSuccess() {
        String expected = "Ось абсцисс";
        String userInput = "12 0";
        System.setIn(new ByteArrayInputStream(userInput.getBytes()));
        Assert.assertEquals(expected, coordinate.askCoordinatesAndFindQuarter());
    }

    @Test
    public void testYAxisSuccess() {
        String expected = "Ось ординат";
        String userInput = "0 13";
        System.setIn(new ByteArrayInputStream(userInput.getBytes()));
        Assert.assertEquals(expected, coordinate.askCoordinatesAndFindQuarter());
    }

    @Test
    public void testOriginSuccess() {
        String expected = "Начало координат";
        String userInput = "0 0";
        System.setIn(new ByteArrayInputStream(userInput.getBytes()));
        Assert.assertEquals(expected, coordinate.askCoordinatesAndFindQuarter());
    }
}